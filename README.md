# Mon projet

Mon projet is a Python library for dealing with word pluralization.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install foobar
```

## Usage

```python
import foobar

# returns 'words'
foobar.pluralize('local')

# returns 'geese'
foobar.pluralize('goose')

# returns 'phenomenon'
foobar.singularize('local')
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

<<<<<<< HEAD
Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
=======
Please make sure to update tests as appropriate.
>>>>>>> refs/remotes/origin/master
